namespace PrepareModules.PubSubModule
{
    /// <summary>
    /// Marker-interface for messages in pub-sub module
    /// </summary>
    public interface IMessage
    {
    }
}