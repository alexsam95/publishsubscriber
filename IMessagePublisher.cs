using System;

namespace PrepareModules.PubSubModule
{
    /// <summary>
    ///  Publisher publish message with type TMessageType.
    /// For start publishing create message,
    /// registrated in messagebroker and invoke OnPublish with message.
    /// </summary>
    public interface IMessagePublisher<out TMessageType> where TMessageType : IMessage
    {
        event Action<TMessageType> OnPublish;
    }
}