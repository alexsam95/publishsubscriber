using System;
using System.Collections;
using System.Collections.Generic;

namespace PrepareModules.PubSubModule
{
    public class MessageBroker : IMessageBroker
    {
        private readonly Dictionary<Type, IList> _subscribeActions =
            new Dictionary<Type, IList>();

        public void AddPublisher<TMessageType>(IMessagePublisher<TMessageType> publisher) where TMessageType : IMessage
        {
            publisher.OnPublish += Published;
        }

        public void SubscribeToMessage<TMessageType>(Action<TMessageType> subscribedAction)
            where TMessageType : IMessage
        {
            var messageType = typeof(TMessageType);
            if (_subscribeActions.ContainsKey(messageType))
            {
                _subscribeActions[messageType].Add(subscribedAction);
            }
            else
            {
                _subscribeActions.Add(messageType, new List<Action<TMessageType>> {subscribedAction});
            }
        }

        public void UnSubscribeToMessage<TMessageType>(Action<TMessageType> action) where TMessageType : IMessage
        {
            _subscribeActions.TryGetValue(typeof(TMessageType), out var subscribers);
            subscribers?.Remove(action);
        }

        private void Published<TMessageType>(TMessageType message) where TMessageType : IMessage
        {
            var messageType = typeof(TMessageType);
            if (!_subscribeActions.ContainsKey(messageType)) return;
            var actions = _subscribeActions[messageType];
            for (var i = 0; i < actions.Count; i++)
            {
                if (actions[i] is Action<TMessageType> action) action.Invoke(message);
            }
        }
    }
}