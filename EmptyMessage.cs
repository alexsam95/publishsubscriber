namespace PrepareModules.PubSubModule
{
    /// <summary>
    /// Message without params (for usual events).
    /// You can create put TEventType for filtration 
    /// </summary>
    public class EmptyMessage <TEventType> : IMessage
    {
        public TEventType Type { get; set; }
        public EmptyMessage(TEventType type)
        {
            Type = type;
        }
    }
}