using System;

namespace PrepareModules.PubSubModule
{
    /// <summary>
    /// Broker - is mediator for work publishers and subscribers
    /// </summary>
    public interface IMessageBroker
    {
        void AddPublisher<TMessageType>(IMessagePublisher<TMessageType> publisher) where TMessageType : IMessage;
        /// <summary>
        /// Subscribe to TMessageType. Action called after publisher send message with this type.
        /// </summary>
        void SubscribeToMessage<TMessageType>(Action<TMessageType> subscribedAction) where TMessageType : IMessage;
        void UnSubscribeToMessage<TMessageType>(Action<TMessageType> action) where TMessageType : IMessage;
    }
}